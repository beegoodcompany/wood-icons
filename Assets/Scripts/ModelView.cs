using System.Collections.Generic;
using UnityEngine;

public class ModelView : MonoBehaviour
{
    public ModelViewType modelViewType;
    public List<MeshRenderer> objectsMesh;
    public float valueOutline = 0.005f;

    public void SetOutlineState(bool state)
    {
        var value = state ? valueOutline : 0;
        
        foreach (var mesh in objectsMesh)
        {
            mesh.material.SetFloat("_OutlineWidth", value);
        }
    }
}