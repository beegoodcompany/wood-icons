using UnityEngine;

public class OffOn : MonoBehaviour
{
    public GameObject canvasObjects;

    public void OnOff()
    {
        canvasObjects.SetActive(!canvasObjects.activeInHierarchy);
    }
}
