﻿using System;
using System.Collections;
using DefaultNamespace.InspectDishes;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class MouseTriggerCheck: MonoBehaviour
    {
        [SerializeField] private BackgroundMove backgroundMove;
        [SerializeField] private DishInspectController dishInspectController;
        [SerializeField] private Animator anim;
        [SerializeField] private Button ButtonBack;

        private ModelView model;
        private bool lockMode;

        void Update()
        {
            if (lockMode)
            {
                return;
            }
            
            if (Input.GetMouseButtonDown(0))
            {
                model = null;
                model = RayToModelView();

                if (model == null)
                {
                    return;
                }
                
                lockMode = true;
                StartCoroutine(PreStartAnimation());
            }
        }

        private IEnumerator PreStartAnimation()
        {
            backgroundMove.freeState = false;
            yield return new WaitForSeconds(0.8f);
            StartAnimation();
            yield return new WaitForSeconds(1f);
            ButtonBack.interactable = true;
            dishInspectController.SetPointToInspect(model.modelViewType);
        }

        private void StartAnimation()
        {
            backgroundMove.lockState = true;
            anim.enabled = true;
            var clipName = GetNameClip();
            anim.SetBool(clipName, true);
            model.SetOutlineState(false);
        }

        public void PreExitViewMod()
        {
            ClearParameters();
            var clipName = GetNameClip();
            anim.SetBool($"{clipName}Back", true);
            StartCoroutine(ExitViewMod());
        }

        private void ClearParameters()
        {
            anim.SetBool("One", false);
            anim.SetBool("Two", false);
            anim.SetBool("Three", false);
            
            anim.SetBool("OneBack", false);
            anim.SetBool("TwoBack", false);
            anim.SetBool("ThreeBack", false);
            
            anim.SetBool("Back", false);
        }
        
        private IEnumerator ExitViewMod()
        {
            dishInspectController.SetPointToInspect(ModelViewType.Three, true);
            dishInspectController.buttonBack.gameObject.SetActive(false);
            yield return new WaitForSeconds(1.5f);
            anim.SetBool("Back", true);
            yield return new WaitForSeconds(0.1f);
            anim.enabled = false;
            model.SetOutlineState(true);
            lockMode = false;

            backgroundMove.freeState = true;
            backgroundMove.lockState = false;
            model = null;
            
            ButtonBack.interactable = false;
            ClearParameters();
        }
        
        private string GetNameClip()
        {
            return Enum.GetName(typeof(ModelViewType), model.modelViewType);
        }
        
        private ModelView RayToModelView()
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.TryGetComponent(out ModelView mv))
                {
                    return mv;
                }
            }

            return null;
        }
    }
}