using UnityEngine;

public class BackgroundUIMove : MonoBehaviour
{
    private Vector2 startPos;
    public float moveSpeed;
    
    private void Start() => startPos = transform.position;

    private void Update()
    {
        Vector2 pz = Camera.main.ScreenToViewportPoint(Input.mousePosition);

        var posX = Mathf.Lerp(transform.position.x, startPos.x + (pz.x * moveSpeed), 2f * Time.deltaTime);
        var posY = Mathf.Lerp(transform.position.y, startPos.y + (pz.y * moveSpeed), 2f * Time.deltaTime);

        transform.position = new Vector3(posX, posY, 0);
    }
}
