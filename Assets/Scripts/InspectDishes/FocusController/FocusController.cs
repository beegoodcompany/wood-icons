using UnityEngine;

public class FocusController : MonoBehaviour
{
    public Camera focusCamera;
    public string FocusedLayer = "Focused";
    
    private GameObject currentlyFocused;
    private int previousLayer;

    private void Update()
    {
        if (currentlyFocused != null)
        {
            gameObject.transform.LookAt(currentlyFocused.transform);
        }
    }
    
    public void EnableFocus(GameObject obj)
    {
        // enables this camera and the postProcessingVolume which is the child
        focusCamera.gameObject.SetActive(true);

        // if something else was focused before reset it
        if (currentlyFocused) currentlyFocused.layer = previousLayer;

        // store and focus the new object
        currentlyFocused = obj;

        if (currentlyFocused)
        {
            previousLayer = currentlyFocused.layer;
            currentlyFocused.layer = LayerMask.NameToLayer(FocusedLayer);
        }
    }

    public void SetFieldOfView(float delta)
    {
        focusCamera.fieldOfView = Mathf.Clamp(focusCamera.fieldOfView - (delta * 5), 30, 60);
    }
    
    // On disable make sure to reset the current object
    public void DisableFocus()
    {
        if (currentlyFocused) currentlyFocused.layer = previousLayer;

        currentlyFocused = null;
        focusCamera.gameObject.SetActive(false);
    }
}
