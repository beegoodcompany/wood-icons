﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace DefaultNamespace.InspectDishes
{
    public class DishInspectController : MonoBehaviour
    {
        [HideInInspector] private Transform pointToInspectObjectCurrent;
        [SerializeField] private Transform pointToInspectObjectOne;
        [SerializeField] private Transform pointToInspectObjectTwo;
        [SerializeField] private Transform pointToInspectObjectThree;
        [SerializeField] private GameObject TriggerOne;
        [SerializeField] private GameObject TriggerTwo;
        [SerializeField] private GameObject TriggerThree;
        [SerializeField] private FocusController focusController;
        [SerializeField] private SwitchObjectsController switchObjectsController;
        [SerializeField] private float rotationSpeed = 5;
        public Button buttonBack;
        private DishInspectView currentDishInspectView;
        private bool ReadyToInspect = false;

        private void Start()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                rotationSpeed = 0.1f;
            }

            if (SceneManager.GetActiveScene().name == "Market")
            {
                buttonBack.gameObject.SetActive(false);
            }
        }

        private void Update()
        {
            if (ReadyToInspect)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    var model = CheckDishInspectViewOnRaycast();
                    if (model == null)
                    {
                        return;
                    }

                    if (currentDishInspectView != null)
                    {
                        currentDishInspectView.BackToSpawnPosition();
                    }

                    currentDishInspectView = model;

                    MoveDishViewToPointToInspect(currentDishInspectView);
                    buttonBack.gameObject.SetActive(true);
                }

                if (Input.GetMouseButton(0) && currentDishInspectView != null)
                {
                    InspectGameObject(currentDishInspectView.gameObject);
                }
            }

            if (Input.GetMouseButton(0) && SceneManager.GetActiveScene().name == "CustomCraft")
            {
                InspectGameObject(switchObjectsController.currentModel.gameObject);
            }


            if (Input.GetAxis("Mouse ScrollWheel") != 0f) // forward
            {
                Camera.main.fieldOfView =
                    Mathf.Clamp(Camera.main.fieldOfView - (Input.GetAxis("Mouse ScrollWheel") * 5), 30, 60);
            }

            if (Input.touchSupported)
            {
                if (Input.touchCount == 2)
                {
                    Touch tZero = Input.GetTouch(0);
                    Touch tOne = Input.GetTouch(1);

                    Vector2 tZeroPrevious = tZero.position - tZero.deltaPosition;
                    Vector2 tOnePrevious = tOne.position - tOne.deltaPosition;

                    float oldTouchDistance = Vector2.Distance(tZeroPrevious, tOnePrevious);
                    float currentTouchDistance = Vector2.Distance(tZero.position, tOne.position);

                    float deltaDistance = oldTouchDistance - currentTouchDistance;
                    Camera.main.fieldOfView =
                        Mathf.Clamp(Camera.main.fieldOfView - (deltaDistance / 10), 30, 60);
                }
            }
        }

        public void ButtonBackToPosition()
        {
            if (ReadyToInspect && currentDishInspectView != null)
            {
                currentDishInspectView.BackToSpawnPosition();
                currentDishInspectView = null;
                buttonBack.gameObject.SetActive(false);
            }
        }

        public void SetDishInspectorView(DishInspectView model)
        {
            switchObjectsController.currentModel = model;
        }

        public void CloseInspectDish()
        {
            switchObjectsController.currentModel.BackToSpawnPosition();
        }

        public void SetReadyToInspect()
        {
            ReadyToInspect = true;
        }

        public void SetNotReadyToInspect()
        {
            ReadyToInspect = false;
        }

        private void InspectGameObject(GameObject gm)
        {
            float rotX = Input.GetAxis("Mouse X") * rotationSpeed * Mathf.Deg2Rad;
            float rotY = Input.GetAxis("Mouse Y") * rotationSpeed * Mathf.Deg2Rad;

            gm.transform.RotateAround(Vector3.up, -rotX);
            gm.transform.RotateAround(Vector3.right, rotY);
        }

        private void MoveDishViewToPointToInspect(DishInspectView model)
        {
            model.gameObject.transform.position = pointToInspectObjectCurrent.position;
        }

        private DishInspectView CheckDishInspectViewOnRaycast()
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.TryGetComponent(out DishInspectView mv))
                {
                    return mv;
                }
            }

            return null;
        }

        public void SetPointToInspect(ModelViewType type, bool isEmpty = false)
        {
            if (isEmpty)
            {
                if (currentDishInspectView != null)
                {
                    currentDishInspectView.BackToSpawnPosition();
                }

                pointToInspectObjectCurrent = null;
                TriggerOne.SetActive(true);
                TriggerTwo.SetActive(true);
                TriggerThree.SetActive(true);
                SetNotReadyToInspect();
                return;
            }

            SetReadyToInspect();
            switch (type)
            {
                case ModelViewType.One:
                {
                    pointToInspectObjectCurrent = pointToInspectObjectOne;
                    TriggerOne.SetActive(false);
                    break;
                }
                case ModelViewType.Two:
                {
                    pointToInspectObjectCurrent = pointToInspectObjectTwo;
                    TriggerTwo.SetActive(false);

                    break;
                }
                case ModelViewType.Three:
                {
                    pointToInspectObjectCurrent = pointToInspectObjectThree;
                    TriggerThree.SetActive(false);
                    break;
                }
            }
        }
    }
}