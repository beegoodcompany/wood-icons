﻿using System;
using DefaultNamespace.Craft;
using UnityEngine;

namespace DefaultNamespace.InspectDishes
{
    public class DishInspectView : MonoBehaviour
    {
        [NonSerialized] public bool isMove;
        public EngraveView engraveView;
        public float speedObj = 10;
        private Vector3 startPosition;
        private Quaternion startRotation;
        private Vector3 positionMoveTo;

        private void Awake()
        {
            startPosition = gameObject.transform.position;
            startRotation = gameObject.transform.rotation;
        }

        public void BackToSpawnPosition()
        {
            gameObject.transform.position = startPosition;
            gameObject.transform.rotation = startRotation;
        }

        public void BackRotation()
        {
            gameObject.transform.rotation = startRotation;
        }

        private void Update()
        {
            if (isMove == true)
            {
                transform.position = Vector3.Lerp(transform.position, positionMoveTo, Time.deltaTime * speedObj);
                
                if (Vector3.Distance(transform.position, positionMoveTo) < 0.01f)
                {
                    transform.position = positionMoveTo;
                    isMove = false;
                }
            }
        }

        public void MoveLerp(Vector3 positionFrom, Vector3 positionTo)
        {
            transform.position = positionFrom;
            positionMoveTo = positionTo;
            isMove = true;
        }

        public void SwapSprite(Sprite sprite)
        {
            engraveView.SetImage(sprite);
        }
    }
}