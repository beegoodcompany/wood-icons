using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ManageScene : MonoBehaviour
{
    public string NameScene;
    public Animator anim;
    
    public void GoToScene()
    {
        anim.SetBool("On", true);
        StartCoroutine(WAIT());
    }

    private IEnumerator WAIT()
    {
        yield return new WaitForSeconds(1.3f);
        LoadScene();
    }
    
    private void LoadScene()
    {
        SceneManager.LoadScene(NameScene);
    }
}
