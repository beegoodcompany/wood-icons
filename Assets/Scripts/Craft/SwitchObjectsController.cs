using System.Collections.Generic;
using DefaultNamespace.Craft;
using DefaultNamespace.InspectDishes;
using UnityEngine;
using UnityEngine.UI;

public class SwitchObjectsController : MonoBehaviour
{
    [HideInInspector] public List<DishInspectView> currentModelsArray;
    [HideInInspector] public DishInspectView currentModel;
    [HideInInspector] public Transform centerPointPosition;

    [SerializeField] private SwitchPrintController switchPrintController;
    
    public List<DishInspectView> dishModels;
    public List<DishInspectView> iconModels;
    
    public Transform centerDishPointPosition;
    public Transform centerIconPointPosition;
    public Transform leftPointPosition;
    public Transform rightPointPosition;
    
    public GameObject dishButton;
    public GameObject iconButton;
    public GameObject light;
    
    private int currentIndex = 0;
    private bool isCanClickCounter;
    
    
    void Start()
    {
        ChangeObjectArray("Dish");
    }

    public void ChangeObjectArray(string type)
    {
        if (currentModel != null && currentModel.isMove)
        {
            return;
        }
        
        switch (type)
        {
            case "Dish":
            {
                light.SetActive(true);
                dishButton.SetActive(true);
                iconButton.SetActive(false);
                
                if (currentModel != null)
                {
                    currentModel.BackToSpawnPosition();
                }

                centerPointPosition = centerDishPointPosition;
                currentModelsArray = dishModels;
                currentModelsArray[0].transform.position = centerPointPosition.position;
                currentModel = currentModelsArray[0];
                currentIndex = 0;

                foreach (var dishInspectView in currentModelsArray)
                {
                    dishInspectView.SwapSprite(switchPrintController.currentSprite.sprite);
                }
                break;
            }
            case "Icon":
            {
                light.SetActive(false);
                dishButton.SetActive(false);
                iconButton.SetActive(true);
                
                if (currentModel != null)
                {
                    currentModel.BackToSpawnPosition();
                }
                
                centerPointPosition = centerIconPointPosition;
                currentModelsArray = iconModels;
                currentModelsArray[0].transform.position = centerPointPosition.position;
                currentModel = currentModelsArray[0];
                currentIndex = 0;
                
                foreach (var dishInspectView in currentModelsArray)
                {
                    dishInspectView.SwapSprite(switchPrintController.currentSprite.sprite);
                }
                break;
            }
        }
    }

    public void PlusCounter()
    {
        if (currentModel.isMove)
        {
            return;
        }
        
        if (currentIndex + 1 < currentModelsArray.Count)
        {
            currentIndex += 1;
        }
        else
        {
            currentIndex = 0;
        }
        
        currentModel.MoveLerp(currentModel.transform.position, rightPointPosition.position);
        currentModel.BackRotation();
        currentModelsArray[currentIndex].MoveLerp(leftPointPosition.position, centerPointPosition.position);
        currentModel = currentModelsArray[currentIndex];
    }
    
    public void MinusCounter()
    {
        if (currentModel.isMove)
        {
            return;
        }
        
        if (currentIndex - 1 >= 0)
        {
            currentIndex -= 1;
        }
        else
        {
            currentIndex = currentModelsArray.Count - 1;
        }
        
        currentModel.MoveLerp(currentModel.transform.position, leftPointPosition.position);
        currentModel.BackRotation();
        currentModelsArray[currentIndex].MoveLerp(rightPointPosition.position, centerPointPosition.position);
        currentModel = currentModelsArray[currentIndex];
    }
}
