﻿using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace.Craft
{
    public class EngraveView : MonoBehaviour
    {
        [SerializeField] private Image image;

        public void SetImage(Sprite sprite)
        {
            image.sprite = sprite;
        }
    }
}