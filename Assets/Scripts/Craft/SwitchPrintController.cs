﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace.Craft
{
    public class SwitchPrintController : MonoBehaviour
    {
        [SerializeField] private SwitchObjectsController switchObjectsController;

        [SerializeField] private List<Sprite> sprites;
        
        public Image currentSprite;
        public GameObject NoneImage;
        
        private int currentIndex;

        private void Start()
        {
            currentSprite.sprite = sprites[0];
            currentIndex = 0;
            
            foreach (var dishModel in switchObjectsController.currentModelsArray)
            {
                dishModel.SwapSprite(sprites[currentIndex]);
            }
        }

        public void PlusCounter()
        {
            if (currentIndex + 1 < sprites.Count)
            {
                currentIndex++;
            }
            else
            {
                currentIndex = 0;
            }

            NoneImage.SetActive(currentIndex == 0);

            currentSprite.sprite = sprites[currentIndex];

            foreach (var dishModel in switchObjectsController.currentModelsArray)
            {
                dishModel.SwapSprite(sprites[currentIndex]);
            }
        }

        public void MinusCounter()
        {
            if(currentIndex - 1 >= 0)
            {
                currentIndex--;
            }
            else
            {
                currentIndex = sprites.Count - 1;
            }
            
            NoneImage.SetActive(currentIndex == 0);
            
            currentSprite.sprite = sprites[currentIndex];
            
            foreach (var dishModel in switchObjectsController.currentModelsArray)
            {
                dishModel.SwapSprite(sprites[currentIndex]);
            }
        }
        
    }
}