using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

public class BackgroundMove : MonoBehaviour
{
    public float RotateSpeed;
    public Transform AnimPoint;
    
    [HideInInspector] public bool freeState = true;
    [HideInInspector] public bool lockState;
    
    private void Update()
    {
        var rotSpeed = RotateSpeed;
        var posHit = Vector3.zero;
        
        if (IsPointerOverUIObject())
        {
            Debug.Log("HIT UI");
            return;
        }
        
        if (freeState)
        {
            var Hit = RayToPoint(out bool isNull);
            
            if (isNull)
            {
                return;
            }
            
            posHit = Hit.point;
            
            if (posHit.x < -60)
            {
                posHit = new Vector3(-60, posHit.y, posHit.z);
            }
        
            if (posHit.x > 40)
            {
                posHit = new Vector3(40, posHit.y, posHit.z);
            }
        
            if (posHit.y < 10)
            {
                posHit = new Vector3(posHit.x, 10f, posHit.z);
            }
        
            if (posHit.y > 50)
            {
                posHit = new Vector3(posHit.x, 50f, posHit.z);
            }
        }
        else
        {
            if (lockState)
            {
                return;
            }

            rotSpeed = 2.3f;
            posHit = AnimPoint.position;
        }
        
        Vector3 direction = posHit - transform.position;
        Quaternion toRotation = Quaternion.FromToRotation(transform.forward, direction);
        transform.rotation = Quaternion.Lerp(transform.rotation, toRotation, rotSpeed * Time.deltaTime);
    }
    
    private bool IsPointerOverUIObject() {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
 
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    private RaycastHit RayToPoint(out bool isNull)
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        
        if (Physics.Raycast(ray, out hit))
        {
            isNull = false;
            return hit;
        }
        else
        {
            isNull = true;
        }
        return hit;//new Vector3(0f, 3.5f,36f);
    }
}
